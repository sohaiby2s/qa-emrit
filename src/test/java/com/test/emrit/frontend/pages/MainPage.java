package com.test.emrit.frontend.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class MainPage extends BasePage {

    @FindBy(xpath = "//img[@src='https://emrit.io/wp-content/uploads/2022/01/desktop-humberger.png']")
    private WebElement signUpTitle;

    @FindBy(xpath = "//a[contains(text(),'SignUp')]")
    private WebElement menuItem;

    public MainPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void clickMenu() {
        signUpTitle.click();
    }

    public void selectMenu() {
        waitUntilElementIsVisible(menuItem);
        waitUntilElementIsClickable(menuItem);
        actionClick(menuItem);
    }
}
