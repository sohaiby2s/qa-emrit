package com.test.emrit.frontend.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmailConfirmPage extends BasePage {

    @FindBy(xpath = "//h1[contains(text(),'Validate your email')]")
    private WebElement signUpTitle;


    public EmailConfirmPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public String getTitleText() {
        waitUntilElementIsVisible(signUpTitle);
        return signUpTitle.getText();
    }

}
