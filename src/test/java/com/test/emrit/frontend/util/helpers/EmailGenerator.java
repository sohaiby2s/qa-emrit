package com.test.emrit.frontend.util.helpers;

import java.time.Instant;

public class EmailGenerator {

    public static String EMAIL_TEMPLATE = "test_challenge_%s@gmail.com";

    public static String getEmail(){
        return String.format(EMAIL_TEMPLATE, getEpochTime());
    }

    private static String getEpochTime(){
        return String.valueOf(Instant.now().toEpochMilli());
    }
}
