# qa-emrit



### Description

This test automation framework used to automate both frontend UI test cases.

### Test cases
- Validate that the user is successfully signing in for different locales
- Validate that the user cannot signUp with same email twice

### Prerequisites
This framework requires following
- Java 14
- Maven
- Chrome browser

Note: This framework is only supporting Chrome browser.

### Steps to execute

#### Clone project 

```
git clone https://gitlab.com/sohaiby2s/qa-emrit.git
```
#### Execution

##### Go to the project path
```
cd {Project-Path}
```

#### Configuration
The framework can be run against multiple environments. The environments are defined in:
```
/src/test/resources/configuration.json
```

##### Execute this command to run
```
mvn clean test -Denv={ENVIRONMENT NAME}
```
{ENVIRONMENT NAME}

This framework is supporting following environments

- dev
- test
- uat
- prod

EXAMPLE COMMAND
mvn clean test -Denv=dev

By default this framework will execute for chrome and test environment
To execute it on default parameters just execute the following command
```
mvn clean test
```

#### Reporting

A detailed extent report has been added after every execution on the following path:

```
TestReport/Test-Automaton-Report.html
```

