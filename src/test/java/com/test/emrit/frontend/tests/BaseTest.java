package com.test.emrit.frontend.tests;

import com.test.emrit.common.ConfigReader;
import com.test.emrit.frontend.util.driver.WebDriverManager;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {


    public WebDriverManager webDriver;


    @BeforeMethod
    public void setup() {
        webDriver = new WebDriverManager();
        webDriver.getWebDriver().get(ConfigReader.URL);
    }

    @AfterMethod
    public void close(ITestResult result) {
        webDriver.closeDriver();
    }

}
